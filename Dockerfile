FROM amd64/ubuntu:hirsute

COPY containers.conf /etc/containers/containers.conf
COPY wsl.conf /etc/wsl.conf
COPY firstboot.sh /bin/firstboot.sh

RUN apt-get update \
    && apt-mark hold systemd \
    && ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata \
    && apt-get install -y \
        git \
        podman \
        sudo \
        wget \
    && apt-get install -y --no-install-recommends \
        python3-pip \
    && rm -r /var/lib/apt/lists \
    && pip3 install --no-cache-dir \
        podman-compose \
    && chmod a+x /bin/firstboot.sh \
    && ln -s /usr/bin/podman /usr/bin/docker \ 
    && ln -s /usr/local/bin/podman-compose /usr/local/bin/docker-compose
