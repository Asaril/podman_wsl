#!/usr/bin/bash

read -p "Creating new user for inside the WSL:" USER
adduser $USER
adduser $USER sudo

echo -e "[user]\ndefault=$USER" > /etc/wsl.conf
