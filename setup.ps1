$DistroTar = 'podman_wsl.tar.gz'
$DistroBasePath = "$env:LocalAppData\podman"

$DistroUri = "https://gitlab.com/Asaril/podman_wsl/-/jobs/artifacts/main/raw/${DistroTar}?job=extract-podman_wsl"

if ([boolean]$($(wsl --list -v) | Select-String "p.o.d.m.a.n. ")) {
    $RemoveOld = Read-Host -Prompt "Podman distro already exists, remove? [y/N]"
    if ($RemoveOld -eq "y") {
        Write-Output "Removing old Podman Distro"
        wsl -t podman
        Start-Sleep -Seconds 2
        wsl --unregister podman
        Start-Sleep -Seconds 2
    } else {
        Write-Error "Cannot continue with existing podman distro, please remove with 'wsl --unregister podman'.`nThis will remove all contained data! To save, export it first with 'wsl --export podman podman.tar'"
    }
}

Write-Output "Downloading Distro"
$OldProgress = $ProgressPreference
$ProgressPreference = 'SilentlyContinue'
Invoke-WebRequest -Uri $DistroUri -OutFile "$env:Temp\$DistroTar" -UseBasicParsing
$ProgressPreference = $OldProgress

Write-Output "Importing Podman Distro"
if (-not $(Test-Path "$DistroBasePath\podman")) {
    New-Item -ItemType "directory" -Path "$DistroBasePath\podman" >/dev/null
}
wsl --import podman "$DistroBasePath\podman" "$env:Temp\$DistroTar"
Start-Sleep -Seconds 1

wsl -d podman /bin/firstboot.sh
Start-Sleep -Seconds 1
wsl -t podman
Start-Sleep -Seconds 2

Write-Output "Finished setup, run distro with 'wsl -d podman'"
