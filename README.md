# podman wsl
This project aims to create a WSL distro for podman, using an Ubuntu 21.04 hirsute docker image as base.

## automatic installation

- Download the setup script: [setup.ps1](https://gitlab.com/Asaril/podman_wsl/-/raw/main/setup.ps1?inline=false)
- Open a powershell and run the downloaded script: `.\setup.ps1`
## manual import

To import: download the .tar.gz from the artifacts at 
[https://gitlab.com/Asaril/podman_wsl/-/jobs/artifacts/main/raw/podman_wsl.tar.gz?job=extract-podman_wsl](https://gitlab.com/Asaril/podman_wsl/-/jobs/artifacts/main/raw/podman_wsl.tar.gz?job=extract-podman_wsl)
and import with:

```powershell
wsl --import podman %localappdata%\podman podman_wsl.tar.gz
wsl -d podman /bin/firstboot.sh
wsl -t podman
```

After running these steps, the new distro can be used with `wsl -d podman`